require 'rails_helper'

RSpec.describe "Potepan::Products_Decorator", type: :model do
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

  describe 'related_products' do
    it "related_products array" do
      expect(product.related_products.distinct).to eq product.related_products
      expect(product.related_products).to eq [related_products[0], related_products[1], related_products[2], related_products[3]]
    end

    it "related_products not product" do
      expect(product.related_products.ids).to_not include product.id
    end
  end
end
