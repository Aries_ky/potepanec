require 'rails_helper'

RSpec.describe 'categories/show.html.erb', type: :request do
  describe 'GET #show' do
    let!(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon, name: "Taxon", taxonomy: taxonomy, parent: taxonomy.root) }
    let(:taxonomy) { create(:taxonomy) }

    before do
      get potepan_category_path(taxon.id)
    end

    it 'responds successfully and 200 response' do
      expect(response).to be_successful
      expect(response).to have_http_status 200
    end

    it 'assigns instance variables' do
      expect(assigns(:taxon)).to eq taxon
    end

    it 'show templete products&taxon display' do
      expect(response.body).to include taxon.name
      expect(response.body).to include taxonomy.name
      expect(response.body).to include product.name
      expect(response.body).to include product.display_price.to_s
    end

    it 'taxon link' do
      expect(response.body).to include potepan_product_path(product.id)
      expect(response.body).to include potepan_category_path(taxon.id)
    end
  end
end
