require 'rails_helper'

RSpec.describe 'products/show.html.erb', type: :request do
  describe 'GET #show' do
    MAX_RELATED_PRODUCTS = Potepan::ProductsController::MAX_RELATED_PRODUCTS
    let(:taxonomy) { create(:taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon, name: "Taxon", taxonomy: taxonomy, parent: taxonomy.root) }
    let!(:related_products) { create_list(:product, MAX_RELATED_PRODUCTS+1, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it 'responds successfully and 200 response' do
      expect(response).to be_successful
      expect(response).to have_http_status 200
    end

    it 'assigns instance variables' do
      expect(controller.instance_variable_get("@product")).to eq product
    end

    it 'show templete products display' do
      expect(response.body).to include product.name
      expect(response.body).to include product.display_price.to_s
      expect(response.body).to include product.description
    end

    it 'product to taxon get link' do
      expect(response.body).to include potepan_category_path(taxon.id)
    end

    it 'related_products display' do
      expect(controller.instance_variable_get("@related_products").size).to eq MAX_RELATED_PRODUCTS
      MAX_RELATED_PRODUCTS.times do |i|
        expect(response.body).to include related_products[i].name
        expect(response.body).to include related_products[i].display_price.to_s
      end
    end
  end
end
