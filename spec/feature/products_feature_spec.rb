require 'rails_helper'

RSpec.feature "products/show.html", type: :feature do
  let(:image) { create_list(:image, 2) }
  let(:variant) { create(:variant, is_master: true, images: [image[0], image[1]]) }
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, master: variant, taxons: [taxon]) }
  let(:related_image) { create(:image) }
  let(:related_variant) { create(:variant, is_master: true, images: [related_image]) }
  let!(:related_product) { create(:product, master: related_variant, taxons: [product.taxons.first], name: "#{product.name}_related") }

  before do
    visit potepan_product_path(product.id)
  end

  scenario "link to home page(Navbar)" do
    within '.navbar-nav' do
      click_link 'Home'
    end
    expect(current_path).to eq potepan_path
  end

  scenario "link to home page(Light-section)" do
    within '#header-light-section' do
      click_link 'Home'
    end
    expect(current_path).to eq potepan_path
  end

  scenario "link to list page" do
    click_link '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  scenario "show templete products display" do
    expect(page).to have_content product.name
    expect(page).to have_content product.price
    expect(page).to have_content product.description
    expect(page).to have_css "img[src*='/spree/products/#{product.images.first.id}/large/#{product.images.first.attachment_file_name}']"
    expect(page).to have_css "img[src*='/spree/products/#{product.images.second.id}/large/#{product.images.second.attachment_file_name}']"
    expect(page).to have_css "img[src*='/spree/products/#{product.images.first.id}/small/#{product.images.first.attachment_file_name}']"
    expect(page).to have_css "img[src*='/spree/products/#{product.images.second.id}/small/#{product.images.second.attachment_file_name}']"
  end

  scenario "show templete related products display" do
    expect(page).to have_content related_product.name
    expect(page).to have_content related_product.price
    expect(page).to have_css "img[src*='#{related_product.display_image.attachment.url}']"
  end

  scenario "link to product page" do
    click_on related_product.name
    expect(current_path).to eq potepan_product_path(related_product.id)
  end
end
